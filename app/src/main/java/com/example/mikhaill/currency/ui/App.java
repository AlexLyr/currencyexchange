package com.example.mikhaill.currency.ui;

import android.app.Application;

import com.example.mikhaill.currency.api.Api;
import com.example.mikhaill.currency.api.gson.deserializer.RateDeserializer;
import com.example.mikhaill.currency.api.model.RateObject;
import com.google.gson.GsonBuilder;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class App extends Application {
    private static App instance;


    public static App getInstance() {
        return instance;
    }

    public Api getApi() {
        return api;
    }

    private Api api;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        createApi();
    }

    private void createApi() {
        api = new Retrofit.Builder()
                .baseUrl("http://api.fixer.io")
                .client(createOkHttpClient())
                .addConverterFactory(createGsonFactory())
                .build()
                .create(Api.class);
    }

    private OkHttpClient createOkHttpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(3, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }

    private GsonConverterFactory createGsonFactory() {
        return GsonConverterFactory.create(
                new GsonBuilder()
                        .registerTypeAdapter(RateObject.class, new RateDeserializer()).create());
    }
}
