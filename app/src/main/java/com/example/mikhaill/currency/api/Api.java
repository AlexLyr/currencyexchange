package com.example.mikhaill.currency.api;

import com.example.mikhaill.currency.api.model.ApiResponse;
import com.example.mikhaill.currency.api.model.RateObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by mikhaill on 08.04.2018.
 */

public interface Api {
    String KEY = "?access_key=05ec63c2c2f5cc7e6375c57aa5257a46";
    @GET("latest"+KEY)
    Call<ApiResponse> getRates(@Query("symbols")String currency);

    @GET("latest"+KEY)
    Call<ApiResponse> getAllRates();


}
