package com.example.mikhaill.currency.api.model;

import java.util.List;

/**
 * Created by mikhaill on 08.04.2018.
 */

public class ApiResponse {

    private List<RateObject> rates;

    public List<RateObject> getRates() {
        return rates;
    }
}
