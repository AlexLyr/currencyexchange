package com.example.mikhaill.currency.api.model;

/**
 * Created by mikhaill on 08.04.2018.
 */

public class RateObject {

    private String name;
    private double rate;

    public RateObject(String name, double rate) {
        this.name = name;
        this.rate = rate;
    }

    public RateObject(RateObject rateObject) {

    }

    public RateObject() {
    }

    public String getName() {
        return name;
    }

    public double getRate() {
        return rate;
    }

    @Override
    public String toString() {
        return "RateObject{" +
                "name='" + name + '\'' +
                ", rate=" + rate +
                '}';
    }
}
