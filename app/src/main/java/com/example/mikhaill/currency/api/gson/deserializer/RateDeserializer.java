package com.example.mikhaill.currency.api.gson.deserializer;

import com.example.mikhaill.currency.api.model.RateObject;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;



public class RateDeserializer implements JsonDeserializer<List<RateObject>> {
    private Gson gson = new Gson();
    @Override
    public List<RateObject> deserialize(
            JsonElement json,
            Type typeOfT,
            JsonDeserializationContext context
    ) throws JsonParseException {
        List<RateObject> rateObjects = new ArrayList<>();
        String[] data = convertStringToRateObjectsFormat(json.toString());
        for (String jsonRow :data) {
            rateObjects.add(new RateObject(gson.fromJson(jsonRow,RateObject.class)));
        }
        return rateObjects;
    }

    private static String[] convertStringToRateObjectsFormat(String jsonString) {
        String rateJsonString = jsonString.substring(jsonString.lastIndexOf("{"),jsonString.length()-1);
        return rateJsonString.replace(",", "}splitter{").split("splitter");
    }
}
